﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Plugin.Settings;
using Xamarin.Forms;
using XamarinFacebookDemo.Model;
using XamarinFacebookDemo.Service;

namespace XamarinFacebookDemo.ViewModel
{
    class MainPageViewModel : INotifyPropertyChanged
    {
        private readonly IFBLoginService _fbLoginService;

        #region UIState

        public FBUser FacebookUser { get; private set; }

        public bool IsLogin
        {
            get
            {
                var status = _fbLoginService.IsLogin;
                if (status && FacebookUser == null)
                {
                    FacebookUser = LoadFBLoginInfo();
                    OnPropertyChanged("FacebookUser");
                }
                return status;
            }
        }

        #endregion

        #region ViewModelCommand

        public ICommand LoginCommand { get; set; }
        public ICommand LogoutCommand { get; set; }

        #endregion

        public MainPageViewModel(IFBLoginService fbLoginService)
        {
            _fbLoginService = fbLoginService;

            WireUpCmd();
        }

        private void WireUpCmd()
        {
            LoginCommand = new Command(FB_login);
            LogoutCommand = new Command(FB_logout);
        }
        private void FB_login()
        {
            _fbLoginService?.Login(OnLoginComplete);
        }

        private void FB_logout()
        {
            _fbLoginService?.Logout();
            FacebookUser = null;
            ClearFBLoginInfo();
            OnPropertyChanged("IsLogin");
            OnPropertyChanged("FacebookUser");
        }


        private void OnLoginComplete(FBUser facebookUser, string message)
        {
            if (facebookUser != null)
            {
                FacebookUser = facebookUser;
                SaveFBLoginInfo(facebookUser);
                OnPropertyChanged("IsLogin");
                OnPropertyChanged("FacebookUser");
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("login failed, reason=" + message);

            }
        }

        private void SaveFBLoginInfo(FBUser fbUser)
        {
            CrossSettings.Current.AddOrUpdateValue($"FB_{nameof(fbUser.Id)}", fbUser.Id);
            CrossSettings.Current.AddOrUpdateValue($"FB_{nameof(fbUser.Token)}", fbUser.Token);
            CrossSettings.Current.AddOrUpdateValue($"FB_{nameof(fbUser.FirstName)}", fbUser.FirstName);
            CrossSettings.Current.AddOrUpdateValue($"FB_{nameof(fbUser.LastName)}", fbUser.LastName);
            CrossSettings.Current.AddOrUpdateValue($"FB_{nameof(fbUser.Email)}", fbUser.Email);
            CrossSettings.Current.AddOrUpdateValue($"FB_{nameof(fbUser.PicUrl)}", fbUser.PicUrl);
        }

        private FBUser LoadFBLoginInfo()
        {
            var id = CrossSettings.Current.GetValueOrDefault($"FB_{nameof(FacebookUser.Id)}", string.Empty);
            var token = CrossSettings.Current.GetValueOrDefault($"FB_{nameof(FacebookUser.Token)}", string.Empty);
            var firstName = CrossSettings.Current.GetValueOrDefault($"FB_{nameof(FacebookUser.FirstName)}", string.Empty);
            var lastName = CrossSettings.Current.GetValueOrDefault($"FB_{nameof(FacebookUser.LastName)}", string.Empty);
            var email = CrossSettings.Current.GetValueOrDefault($"FB_{nameof(FacebookUser.Email)}", string.Empty);
            var picUrl = CrossSettings.Current.GetValueOrDefault($"FB_{nameof(FacebookUser.PicUrl)}", string.Empty);

            return new FBUser
            {
                Id = id,
                Token = token,
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                PicUrl = picUrl
            };
        }

        private void ClearFBLoginInfo()
        {
            CrossSettings.Current.Clear($"FB_{nameof(FacebookUser.Id)}");
            CrossSettings.Current.Clear($"FB_{nameof(FacebookUser.Token)}");
            CrossSettings.Current.Clear($"FB_{nameof(FacebookUser.FirstName)}");
            CrossSettings.Current.Clear($"FB_{nameof(FacebookUser.LastName)}");
            CrossSettings.Current.Clear($"FB_{nameof(FacebookUser.Email)}");
            CrossSettings.Current.Clear($"FB_{nameof(FacebookUser.PicUrl)}");
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
